# Fixings
*the 'g' is silent*

`1.2.0`

[![pipeline status](https://gitlab.com/Drew-S/fixings/badges/master/pipeline.svg)](https://gitlab.com/Drew-S/fixings/commits/master) [![coverage report](https://gitlab.com/Drew-S/fixings/badges/master/coverage.svg)](https://gitlab.com/Drew-S/fixings/commits/master)

Add a plugin system to your project with ease.

**Install**

`npm install --save fixings`

```javascript
const fixings = require('fixings')

// Create a new plugin manager
var Plate = new Fixings()

// Add some plugins
Plate.addPlugin({
  name: 'my-plugin',
  'hook': function(...) {
    // do stuff when called upon
  }
})
Plate.addPlugin(require('some-plugin'))

// Create a consumer for a specific hook
var Eater = Plate.resolveHook('hook')

// Loop through the plugins and run them
do {
  var plugin = Eater.next()
  if (plugin !== null)
    plugin.handler(...)
} while (plugin)
```
For full documentation and tutorials (soon)(tm) see https://drew-s.gitlab.io/fixings
