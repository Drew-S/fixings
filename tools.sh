#!/bin/bash

if [ "$1" == "readme" ]; then
  cp README.template.md README.md
  VERSION=`node -p "require('./package.json').version"`
  sed -i "s/{{NPM VERSION}}/${VERSION}/g" README.md
fi
