var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate = new Fixings()
var plugin1 = {
  name: 'test1',
  'hook': function() { return true }
}
var plugin2 = {
  name: 'test2',
  dependencies: ['test1'],
  'hook': function() { return true }
}

it('Plate.addPlugin() should add plugin without throwing error', () => {
  expect(() => { Plate.addPlugin(plugin1) }).to.not.throw()
})

it('Plate.addPlugin() should add plugin without throwing error', () => {
  expect(() => { Plate.addPlugin(plugin2) }).to.not.throw()
})

it('Plate.verifyDependencies() should return an empty list (no missing dependencies)', () => {
  expect(Plate.verifyDependencies()).to.have.lengthOf(0)
})
