var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate = new Fixings()
var plugin = {
  name: 'test',
  'hook': function() { return true }
}

it('Plate.addPlugin() should add the plugin without error, returning true', () => {
  expect(() => { Plate.addPlugin(plugin) }).to.not.throw().and.to.be.true
})

it('Plate.addPlugin() should not add the plugin without error, returning false', () => {
  expect(() => { Plate.addPlugin(plugin) }).to.not.throw().and.to.be.false
})

it('Plate.getPlugins() should return a list of 1 plugin', () => {
  expect(Plate.getPlugins()).to.have.lengthOf(1)
})
