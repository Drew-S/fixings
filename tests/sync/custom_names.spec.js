var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate
it('Plate should be created without throwing an error', () => {
  expect(() => { Plate = new Fixings({
    name: 'customName',
    handler: 'customHandler',
    before: 'customBefore',
    after: 'customAfter',
    dependencies: 'customDependencies'
  })}).to.not.throw()
})

//
// ==== CHECK IF NAME VARIABLES ARE CHANGED ====================================
//

it('Plate.$name should be "customName"', () => {
  expect(Plate.$name).to.equal('customName')
})

it('Plate.$handler should be "customHandler"', () => {
  expect(Plate.$handler).to.equal('customHandler')
})

it('Plate.$before should be "customBefore"', () => {
  expect(Plate.$before).to.equal('customBefore')
})

it('Plate.$after should be "customAfter"', () => {
  expect(Plate.$after).to.equal('customAfter')
})

it('Plate.$dependencies should be "customDependencies"', () => {
  expect(Plate.$dependencies).to.equal('customDependencies')
})

describe('Adding Plugins to a Plate', () => {
  var plugin1 = {
    customName: 'test',
    'hook': function() { return true },
    'ordered-after-hook': {
      customAfter: ['test2'],
      customHandler: function() { return 'second' }
    },
    'ordered-before-hook': {
      customBefore: ['test2'],
      customHandler: function() { return 'first' }
    }
  }
  var plugin2 = {
    customName: 'test2',
    'hook': function() { return true },
    'ordered-after-hook': function() { return 'first' },
    'ordered-before-hook': function() { return 'second' }
  }
  describe('Plugins should be added without error', () => {
    it('Plate.addPlugin(plugin1) should not throw an error', () => {
      expect(() => { Plate.addPlugin(plugin1) }).to.not.throw().and.to.be.true
    })

    it('Plate.addPlugin(plugin2) should not throw an error', () => {
      expect(() => { Plate.addPlugin(plugin2) }).to.not.throw().and.to.be.true
    })

    it('Plate.getPlugins() should return an array of installed plugins', () => {
      expect(Plate.getPlugins()).to.be.an('array').that.includes('test')
      expect(Plate.getPlugins()).to.be.an('array').that.includes('test2')
    })
  })
})

it('Place.verifyDependencies() should return an empty list', () => {
  expect(Plate.verifyDependencies()).to.have.lengthOf(0)
})

describe('Consuming "hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in any order', () => {
    plugin1 = Eat.next()
    expect(plugin1.customName).to.satisfy(v => ['test', 'test2'].includes(v))

  })

  it('Plugin.customHandler() should return true', () => {
    expect(plugin1.customHandler()).to.be.true
  })

  it('Eat.next() call each plugin, in any order', () => {
    plugin2 = Eat.next()
    expect(plugin2.customName).to.satisfy(v => ['test', 'test2'].includes(v))

  })

  it('Plugin.customHandler() should return true', () => {
    expect(plugin2.customHandler()).to.be.true
  })
})

describe('Consuming "ordered-after-hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('ordered-after-hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in a specific order, "test2" first', () => {
    plugin1 = Eat.next()
    expect(plugin1.customName).to.equal('test2')
  })

  it('Plugin.customHandler() should be "first"', () => {
    expect(plugin1.customHandler()).to.equal('first')
  })

  it('Eat.next() should call each plugin, in a specific order, "test" second', () => {
    plugin2 = Eat.next()
    expect(plugin2.customName).to.equal('test')
  })

  it('Plugin.customHandler() should be "second"', () => {
    expect(plugin2.customHandler()).to.equal('second')
  })
})

describe('Consuming "ordered-before-hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('ordered-before-hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in a specific order, "test" first', () => {
    plugin1 = Eat.next()
    expect(plugin1.customName).to.equal('test')
  })

  it('Plugin.customHandler() should be "first"', () => {
    expect(plugin1.customHandler()).to.equal('first')
  })

  it('Eat.next() should call each plugin, in a specific order, "test2" second', () => {
    plugin2 = Eat.next()
    expect(plugin2.customName).to.equal('test2')
  })

  it('Plugin.customHandler() should be "second"', () => {
    expect(plugin2.customHandler()).to.equal('second')
  })
})
