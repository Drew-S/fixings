var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate = new Fixings()
var plugin1 = {
  name: 'test1',
  dependencies: ['test3', 'test2', 'test5'],
  'hook': function() { return true }
}
var plugin2 = {
  name: 'test2',
  dependencies: ['test3'],
  'hook': function() { return true }
}

it('Plate.addPlugin() should add a plugin without throwing an error', () => {
  expect(() => { Plate.addPlugin(plugin1) }).to.not.throw()
})

it('Plate.addPlugin() should add a plugin without throwing an error', () => {
  expect(() => { Plate.addPlugin(plugin2) }).to.not.throw()
})

var missing

it('Plate.verifyDependencies() to not throw an error, returning Missing array', () => {
  expect(() => { missing = Plate.verifyDependencies() }).to.not.throw()
})

it('Missing should be an array with a length of 2', () => {
  expect(missing).to.have.lengthOf(2)
})

it('Missing should contain the plugin "test3"', () => {
  expect(missing).to.include('test3')
})

it('Missing should contain the plugin "test5"', () => {
  expect(missing).to.include('test5')
})
