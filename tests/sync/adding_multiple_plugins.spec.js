var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate = new Fixings()
var max = 5

var results
var plugins = []
for (var i=0; i<max; i++) {
  plugins.push({
    name: `test${i}`,
    'hook': function() { return `test${i}` }
  })
}

it('Plate.addPlugins() should run without error returning Results', () => {
  expect(() => { results = Plate.addPlugins(plugins) }).to.not.throw()
})

it('Results should be an Array of length 5', () => {
  expect(results).to.be.an('Array')
  expect(results).to.have.lengthOf(5)
})

it('Results[i] should each contain success values of true', () => {
  for (var i=0; i<max; i++) {
    expect(results[i].success).to.be.true
  }
})

it(`Plate.getPlugins() should return ${max} unique plugins`, () => {
  var plugs = Plate.getPlugins()
  expect(plugs).to.have.lengthOf(max)
  for (var i=0; i<max; i++) {
    expect(plugs[i]).to.equal(`test${i}`)
  }
})
