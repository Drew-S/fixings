var expect = require('chai').expect
var Fixings = require('../../index.js')

describe('Creating a new Fixings with incorrect options', () => {
  describe('new Fixings(options) with non-object or non-null options should throw TypeError', () => {
    it('~string should throw TypeError', () => {
      expect(() => { new Fixings('wrong') }).to.throw(TypeError)
    })
    it('~array should throw TypeError', () => {
      expect(() => { new Fixings(['wrong']) }).to.throw(TypeError)
    })
    it('~number should throw TypeError', () => {
      expect(() => { new Fixings(1234) }).to.throw(TypeError)
    })
    it('~function should throw TypeError', () => {
      expect(() => { new Fixings(function() { return 'wrong' }) }).to.throw(TypeError)
    })
    it('~boolean should throw TypeError', () => {
      expect(() => { new Fixings(true) }).to.throw(TypeError)
      expect(() => { new Fixings(false) }).to.throw(TypeError)
    })
    it('~null should not throw TypeError', () => {
      expect(() => { new Fixings(null) }).to.not.throw()
    })
    it('~object should not throw TypeError', () => {
      expect(() => { new Fixings({}) }).to.not.throw()
    })
    it('~undefined should not throw TypeError', () => {
      expect(() => { new Fixings() }).to.not.throw()
    })

  })

  describe('new Fixings(options) with non-string or non-null properties should throw TypeError', () => {
    describe('options.name', () => {
      it('~boolean should throw TypeError', () => {
        expect(() => { new Fixings({ name: false }) }).to.throw(TypeError)
        expect(() => { new Fixings({ name: true }) }).to.throw(TypeError)
      })
      it('~array should throw TypeError', () => {
        expect(() => { new Fixings({ name: ['wrong'] }) }).to.throw(TypeError)
      })
      it('~number should throw TypeError', () => {
        expect(() => { new Fixings({ name: 1234 }) }).to.throw(TypeError)
      })
      it('~function should throw TypeError', () => {
        expect(() => { new Fixings({ name: function() { return 'wrong' } }) }).to.throw(TypeError)
      })
      it('~null should not throw TypeError', () => {
        expect(() => { new Fixings({ name: null }) }).to.not.throw()
      })
      it('~string should not throw TypeError', () => {
        expect(() => { new Fixings({ name: 'name' }) }).to.not.throw()
      })
    })

    describe('options.before', () => {
      it('~boolean should throw TypeError', () => {
        expect(() => { new Fixings({ before: false }) }).to.throw(TypeError)
        expect(() => { new Fixings({ before: true }) }).to.throw(TypeError)
      })
      it('~array should throw TypeError', () => {
        expect(() => { new Fixings({ before: ['wrong'] }) }).to.throw(TypeError)
      })
      it('~number should throw TypeError', () => {
        expect(() => { new Fixings({ before: 1234 }) }).to.throw(TypeError)
      })
      it('~function should throw TypeError', () => {
        expect(() => { new Fixings({ before: function() { return 'wrong' } }) }).to.throw(TypeError)
      })
      it('~null should not throw TypeError', () => {
        expect(() => { new Fixings({ before: null }) }).to.not.throw()
      })
      it('~string should not throw TypeError', () => {
        expect(() => { new Fixings({ before: 'before' }) }).to.not.throw()
      })
    })

    describe('options.after', () => {
      it('~boolean should throw TypeError', () => {
        expect(() => { new Fixings({ after: false }) }).to.throw(TypeError)
        expect(() => { new Fixings({ after: true }) }).to.throw(TypeError)
      })
      it('~array should throw TypeError', () => {
        expect(() => { new Fixings({ after: ['wrong'] }) }).to.throw(TypeError)
      })
      it('~number should throw TypeError', () => {
        expect(() => { new Fixings({ after: 1234 }) }).to.throw(TypeError)
      })
      it('~function should throw TypeError', () => {
        expect(() => { new Fixings({ after: function() { return 'wrong' } }) }).to.throw(TypeError)
      })
      it('~null should not throw TypeError', () => {
        expect(() => { new Fixings({ after: null }) }).to.not.throw()
      })
      it('~string should not throw TypeError', () => {
        expect(() => { new Fixings({ after: 'after' }) }).to.not.throw()
      })
    })

    describe('options.handler', () => {
      it('~boolean should throw TypeError', () => {
        expect(() => { new Fixings({ handler: false }) }).to.throw(TypeError)
        expect(() => { new Fixings({ handler: true }) }).to.throw(TypeError)
      })
      it('~array should throw TypeError', () => {
        expect(() => { new Fixings({ handler: ['wrong'] }) }).to.throw(TypeError)
      })
      it('~number should throw TypeError', () => {
        expect(() => { new Fixings({ handler: 1234 }) }).to.throw(TypeError)
      })
      it('~function should throw TypeError', () => {
        expect(() => { new Fixings({ handler: function() { return 'wrong' } }) }).to.throw(TypeError)
      })
      it('~null should not throw TypeError', () => {
        expect(() => { new Fixings({ handler: null }) }).to.not.throw()
      })
      it('~string should not throw TypeError', () => {
        expect(() => { new Fixings({ handler: 'handler' }) }).to.not.throw()
      })
    })

    describe('options.dependencies', () => {
      it('~boolean should throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: false }) }).to.throw(TypeError)
        expect(() => { new Fixings({ dependencies: true }) }).to.throw(TypeError)
      })
      it('~array should throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: ['wrong'] }) }).to.throw(TypeError)
      })
      it('~number should throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: 1234 }) }).to.throw(TypeError)
      })
      it('~function should throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: function() { return 'wrong' } }) }).to.throw(TypeError)
      })
      it('~null should not throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: null }) }).to.not.throw()
      })
      it('~string should not throw TypeError', () => {
        expect(() => { new Fixings({ dependencies: 'dependencies' }) }).to.not.throw()
      })
    })
  })

  describe('new Fixings(options) with colliding values', () => {
    it('options.name and options.dependencies having same value should throw error', () => {
      expect(() => { new Fixings({ name: 'same', dependencies: 'same' }) }).to.throw(Error)
    })

    it('options.before and options.after having same value should throw error', () => {
      expect(() => { new Fixings({ before: 'same', after: 'same' }) }).to.throw(Error)
    })

    it('options.before and options.handler having same value should throw error', () => {
      expect(() => { new Fixings({ before: 'same', handler: 'same' }) }).to.throw(Error)
    })

    it('options.after and options.handler having same value should throw error', () => {
      expect(() => { new Fixings({ after: 'same', handler: 'same' }) }).to.throw(Error)
    })

    it('options.after and options.handler and options.before having same value should throw error', () => {
      expect(() => { new Fixings({ after: 'same', handler: 'same', before: 'same' }) }).to.throw(Error)
    })
  })
})

describe('Adding a plugin without correct parameters', () => {
  var Plate = new Fixings()
  describe('Plate.addPlugin() should throw errors for incorrect object types', () => {
    it('~undefined should throw TypeError', () => {
      expect(() => { Plate.addPlugin() }).to.throw(TypeError)
    })
    it('~string should throw TypeError', () => {
      expect(() => { Plate.addPlugin('wrong') }).to.throw(TypeError)
    })
    it('~array should throw TypeError', () => {
      expect(() => { Plate.addPlugin(['wrong']) }).to.throw(TypeError)
    })
    it('~number should throw TypeError', () => {
      expect(() => { Plate.addPlugin(123) }).to.throw(TypeError)
    })
    it('~boolean should throw TypeError', () => {
      expect(() => { Plate.addPlugin(true) }).to.throw(TypeError)
      expect(() => { Plate.addPlugin(false) }).to.throw(TypeError)
    })
    it('~null should throw TypeError', () => {
      expect(() => { Plate.addPlugin(null) }).to.throw(TypeError)
    })
    it('~function should throw TypeError', () => {
      expect(() => { Plate.addPlugin(function() { return 'wrong' }) }).to.throw(TypeError)
    })
    it('~string should not throw TypeError', () => {
      expect(() => { Plate.addPlugin({ name: 'test' }) }).to.not.throw()
    })
  })

  it('Plate.addPlugin() should throw an error for missing plugin name', () => {
    expect(() => { Plate.addPlugin({ 'hook': function() { return true }}) }).to.throw(Error)
  })
})

describe('Adding plugins without correct parameters', () => {
  var Plate = new Fixings()
  it('Plate.addPlugins() should throw errors for incorrect object types', () => {
    it('~undefined should throw TypeError', () => {
      expect(() => { Plate.addPlugins() }).to.throw(TypeError)
    })
    it('~string should throw TypeError', () => {
      expect(() => { Plate.addPlugins('wrong') }).to.throw(TypeError)
    })
    it('~object should throw TypeError', () => {
      expect(() => { Plate.addPlugins({}) }).to.throw(TypeError)
    })
    it('~number should throw TypeError', () => {
      expect(() => { Plate.addPlugins(123) }).to.throw(TypeError)
    })
    it('~null should throw TypeError', () => {
      expect(() => { Plate.addPlugins(null) }).to.throw(TypeError)
    })
    it('~boolean should throw TypeError', () => {
      expect(() => { Plate.addPlugins(true) }).to.throw(TypeError)
      expect(() => { Plate.addPlugins(false) }).to.throw(TypeError)
    })
    it('~function should throw TypeError', () => {
      expect(() => { Plate.addPlugins(function() { return 'wrong' }) }).to.throw(TypeError)
    })
    it('~array should not throw TypeError', () => {
      expect(() => { Plate.addPlugins([{ name: 'test' }]) }).to.throw(TypeError)
    })
  })
})

describe('Resolving hook with incorrect parameters', () => {
  var Plate = new Fixings()
  it('Plate.resolveHook() should throw errors for incorrect object types', () => {
    it('~undefined should throw TypeError', () => {
      expect(() => { Plate.resolveHook() }).to.throw(TypeError)
    })
    it('~object should throw TypeError', () => {
      expect(() => { Plate.resolveHook({}) }).to.throw(TypeError)
    })
    it('~array should throw TypeError', () => {
      expect(() => { Plate.resolveHook(['wrong']) }).to.throw(TypeError)
    })
    it('~boolean should throw TypeError', () => {
      expect(() => { Plate.resolveHook(true) }).to.throw(TypeError)
      expect(() => { Plate.resolveHook(false) }).to.throw(TypeError)
    })
    it('~null should throw TypeError', () => {
      expect(() => { Plate.resolveHook(null) }).to.throw(TypeError)
    })
    it('~function should throw TypeError', () => {
      expect(() => { Plate.resolveHook(function() { return 'wrong' }) }).to.throw(TypeError)
    })
  })

  it('Plate.resolveHook() should throw an error for non-existant hook name', () => {
    expect(() => { Plate.resolveHook('nope') }).to.throw(Error)
  })
})
