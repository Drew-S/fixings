var expect = require('chai').expect
var Fixings = require('../../index.js')

var Plate
it('Plate should be created without throwing an error', () => {
  expect(() => { Plate = new Fixings()}).to.not.throw()
})

it('Plate.$name should be "name"', () => {
  expect(Plate.$name).to.equal('name')
})

it('Plate.$handler should be "handler"', () => {
  expect(Plate.$handler).to.equal('handler')
})

it('Plate.$before should be "before"', () => {
  expect(Plate.$before).to.equal('before')
})

it('Plate.$after should be "after"', () => {
  expect(Plate.$after).to.equal('after')
})

it('Plate.$dependencies should be "dependencies"', () => {
  expect(Plate.$dependencies).to.equal('dependencies')
})

describe('Adding Plugins to a Plate', () => {
  var plugin1 = {
    name: 'test',
    'hook': function() { return true },
    'ordered-after-hook': {
      after: ['test2'],
      handler: function() { return 'second' }
    },
    'ordered-before-hook': {
      before: ['test2'],
      handler: function() { return 'first'}
    }
  }
  var plugin2 = {
    name: 'test2',
    'hook': function() { return true },
    'ordered-after-hook': function() { return 'first' },
    'ordered-before-hook': function() { return 'second'}
  }
  describe('Plugins should be added without error', () => {
    it('Plate.addPlugin() should not throw an error', () => {
      expect(() => { Plate.addPlugin(plugin1) }).to.not.throw().and.to.be.true
    })

    it('Plate.addPlugin() should not throw an error', () => {
      expect(() => { Plate.addPlugin(plugin2) }).to.not.throw().and.to.be.true
    })

    it('Plate.getPlugins() should return an array of installed plugins', () => {
      expect(Plate.getPlugins()).to.be.an('array').that.includes('test')
      expect(Plate.getPlugins()).to.be.an('array').that.includes('test2')
    })
  })
})

it('Place.verifyDependencies() should return an empty list', () => {
  expect(Plate.verifyDependencies()).to.have.lengthOf(0)
})

describe('Consuming "hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in any order', () => {
    plugin1 = Eat.next()
    expect(plugin1.name).to.satisfy((v) => { return ['test', 'test2'].indexOf(v) !== -1 })

  })

  it('Plugin.handler() should return true', () => {
    expect(plugin1.handler()).to.be.true
  })

  it('Eat.next() should call each plugin, in any order', () => {
    plugin2 = Eat.next()
    expect(plugin2.name).to.satisfy((v) => { return ['test', 'test2'].indexOf(v) !== -1 })

  })

  it('Plugin.handler() should return true', () => {
    expect(plugin2.handler()).to.be.true
  })
})

describe('Consuming "ordered-after-hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('ordered-after-hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in a specific order, "test2" first', () => {
    plugin1 = Eat.next()
    expect(plugin1.name).to.equal('test2')
  })

  it('Plugin.handler() should be "first"', () => {
    expect(plugin1.handler()).to.equal('first')
  })

  it('Eat.next() should call each plugin, in a specific order, "test" second', () => {
    plugin2 = Eat.next()
    expect(plugin2.name).to.equal('test')
  })

  it('Plugin.handler() should be "second"', () => {
    expect(plugin2.handler()).to.equal('second')
  })
})

describe('Consuming "ordered-before-hook"', () => {
  var Eat
  var plugin1
  var plugin2
  it('Plate.resolveHook(...) should create an Eat (consumer) without throwing an error', () => {
    expect(() => { Eat = Plate.resolveHook('ordered-before-hook') }).to.not.throw()
  })

  it('Eat.next() should call each plugin, in a specific order, "test" first', () => {
    plugin1 = Eat.next()
    expect(plugin1.name).to.equal('test')
  })

  it('Plugin.handler() should be "first"', () => {
    expect(plugin1.handler()).to.equal('first')
  })

  it('Eat.next() should call each plugin, in a specific order, "test2" second', () => {
    plugin2 = Eat.next()
    expect(plugin2.name).to.equal('test2')
  })

  it('Plugin.handler() should be "second"', () => {
    expect(plugin2.handler()).to.equal('second')
  })
})
