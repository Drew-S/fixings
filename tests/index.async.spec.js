describe('Callback', () => {
  require('./async/callback.spec')
})

describe('Promises (Async and Await)', () => {
  require('./async/promise.spec')
})
