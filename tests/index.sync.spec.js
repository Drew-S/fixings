
describe('Syncronous', () => {
  // DEFAULT NAMES (AND RUNNING)
  describe('New default plugin Plate (with running operations)', () => {
    require('./sync/default_names.spec')
  })

  // CUSTOM NAMES (AND RUNNING)
  describe('New plugin Plate with custom names (with running operations)', () => {
    require('./sync/custom_names.spec')
  })


  // ADDING MULTIPLE PLUGINS AT ONCE
  describe('Add multiple plugins at once', () => {
    require('./sync/adding_multiple_plugins.spec')
  })

  // ADDING THE SAME PLUGIN TWICE
  describe('Adding the same plugin twice', () => {
    require('./sync/adding_same_plugin.spec')
  })

  // CHECKING DEPENDENCIES
  describe('Adding plugins with fulfilled dependencies', () => {
    require('./sync/fulfilled_dependencies.spec')
  })

  describe('Adding plugins with unfulfilled dependencies', () => {
    require('./sync/unfulfilled_dependencies.spec')
  })

  // ERROR CHECKING
  describe('Throwing errors when invalid data is provided', () => {
    require('./sync/error_checks.spec')
  })
})
