var expect = require('chai').expect
var Fixings = require('../../index')

var plugin1 = {
  name: 'test1',
  'hook': function() { return 'test1' },
  dependencies: ['test2']
}
var plugin2 = {
  name: 'test2',
  'hook': function() { return 'test2' },
  dependencies: ['test3']
}
var plugins = [
  {
    name: 'test3',
    'hook': function() { return 'test3' }
  },
  {
    name: 'test4',
    'hook': function() { return 'test4' }
  }
]

var Plate
before(() => {
  Plate = new Fixings()
})

describe('Adding two plugins (one at a time)', () => {
  it('Plate.addPluginAsync() should add a plugin without error', (done) => {
    var result = Plate.addPluginAsync(plugin1)
    expect(result).to.be.a('Promise')
    result.then(r => {
      expect(r).to.be.true
      done()
    })
  })

  it('Plate.addPluginAsync() should add a plugin without error', (done) => {
    var result = Plate.addPluginAsync(plugin2)
    expect(result).to.be.a('Promise')
    result.then(r => {
      expect(r).to.be.true
      done()
    })
  })
})

describe('Adding multiple plugins at the same time', () => {
  it('Should add two plugins (at the same time)', (done) => {
    var results = Plate.addPluginsAsync(plugins)
    expect(results).to.be.a('Promise')
    results.then(r => {
      expect(r).to.be.true
      done()
    })
  })
})

describe('Get a list of plugins', () => {
  it('Plate.getPluginsAsync()', (done) => {
    var results = Plate.getPluginsAsync()
    results.then(plugins => {
      expect(plugins).to.be.lengthOf(4)
      expect(plugins).to.contain('test1')
      expect(plugins).to.contain('test2')
      expect(plugins).to.contain('test3')
      expect(plugins).to.contain('test4')
      done()
    })
  })
})

describe('Running the plugins', () => {
  var Eat
  it('Plate.resolveHookAsync() should create a new Eat class without error', (done) => {
    var result = Plate.resolveHookAsync('hook')
    expect(result).to.be.a('Promise')
    result.then(eater => {
      expect(eater).to.contain.property('hook')
        .with.property('plugins')
        .with.a.lengthOf(4)
      Eat = eater
      done()
    })
  })

  for(var i=0; i<4; i++) {
    it('Eat.nextAsync() should iterate over the plugins', (done) => {
      var result = Eat.nextAsync()
      expect(result).to.be.a('Promise')
      result.then(plugin => {
        expect(plugin.handler()).to.be.oneOf(['test1', 'test2', 'test3', 'test4'])
        done()
      })
    })
  }
})

describe('Verifying dependencies', () => {
  it('Plate.verifyDependenciesAsync() should return an empty list', (done) => {
    var result = Plate.verifyDependenciesAsync()
    expect(result).to.be.a('Promise')
    result.then(missing => {
      expect(missing).to.be.empty
      done()
    })
  })

  it('Plate.verifyDependenciesAsync() should return a list with missing plugins', (done) => {
    Plate.addPlugin({
      name: 'test5',
      'hook': function() { return true },
      dependencies: ['non-existant-plugin']
    })
    var result = Plate.verifyDependenciesAsync()
    expect(result).to.be.a('Promise')
    result.then(missing => {
      expect(missing).to.have.lengthOf(1)
      expect(missing).to.contain('non-existant-plugin')
      done()
    })
  })
})

describe('Error checking for incorrect values', () => {
  describe('Plate.addPluginAsync() should throw where plugin is not an object', () => {
    it('~function should throw TypeError', (done) => {
      Plate.addPluginAsync(function() {})
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~string should throw TypeError', (done) => {
      Plate.addPluginAsync('string')
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~number should throw TypeError', (done) => {
      Plate.addPluginAsync(123)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.addPluginAsync(true)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          Plate.addPluginAsync(false)
            .catch(error => {
              expect(error).to.be.an.instanceof(TypeError)
              done()
            })
        })
    })

    it('~null should throw TypeError', (done) => {
      Plate.addPluginAsync(null)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~array should throw TypeError', (done) => {
      Plate.addPluginAsync(['wrong'])
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.addPluginAsync(undefined)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~object should not throw TypeError', (done) => {
      Plate.addPluginAsync({ name: 'test' })
        .then(() => {
          // Not forcing, just no rejection so no error occured.
          expect(true).to.be.true
          done()
        })
    })
  })

  describe('Plate.addPluginsAsync() should throw where plugins is not an array', () => {
    it('~string should throw TypeError', (done) => {
      Plate.addPluginsAsync('wrong')
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~number should throw TypeError', (done) => {
      Plate.addPluginsAsync(123)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~object should throw TypeError', (done) => {
      Plate.addPluginsAsync({})
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.addPluginsAsync(false)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          Plate.addPluginsAsync(true)
            .catch(error => {
              expect(error).to.be.an.instanceof(TypeError)
              done()
            })
        })
    })

    it('~function should throw TypeError', (done) => {
      Plate.addPluginsAsync(function() {})
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~null should throw TypeError', (done) => {
      Plate.addPluginsAsync(null)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.addPluginsAsync(undefined)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~array should not throw TypeError', (done) => {
      Plate.addPluginsAsync([{ name: 'test' }])
        .then(() => {
          // Not forcing, just no rejection so no error occured.
          expect(true).to.be.true
          done()
        })
    })
  })

  describe('Plate.resolveHookAsync() should throw where hook is not a string', () => {
    it('~null should throw TypeError', (done) => {
      Plate.resolveHookAsync(null)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.resolveHookAsync(undefined)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.resolveHookAsync(true)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          Plate.resolveHookAsync(false)
            .catch(error => {
              expect(error).to.be.an.instanceof(TypeError)
              done()
            })
        })
    })

    it('~object should throw TypeError', (done) => {
      Plate.resolveHookAsync({})
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~array should throw TypeError', (done) => {
      Plate.resolveHookAsync(['wrong'])
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~number should throw TypeError', (done) => {
      Plate.resolveHookAsync(123)
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })

    it('~function should throw TypeError', (done) => {
      Plate.resolveHookAsync(function() {})
        .catch(error => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
    })
  })
})
