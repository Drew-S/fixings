var expect = require('chai').expect
var Fixings = require('../../index')

var plugin1 = {
  name: 'test1',
  'hook': function() { return 'test1' },
  dependencies: ['test2']
}
var plugin2 = {
  name: 'test2',
  'hook': function() { return 'test2' },
  dependencies: ['test3']
}
var plugins = [
  {
    name: 'test3',
    'hook': function() { return 'test3' }
  },
  {
    name: 'test4',
    'hook': function() { return 'test4' }
  }
]

var Plate

before(() => {
  Plate = new Fixings()
})

describe('Adding plugins (one at a time) without error', () => {
  it('Plate.addPluginAsync() should add plugin without error', (done) => {
    Plate.addPluginAsync(plugin1, (error, results) => {
      expect(error).to.be.null
      expect(results).to.be.true
      done()
    })
  })

  it('Plate.addPluginAsync() should add plugin without error', (done) => {
    Plate.addPluginAsync(plugin2, (error, results) => {
      expect(error).to.be.null
      expect(results).to.be.true
      done()
    })
  })
})

describe('Adding multiple plugins at the same time', () => {
  it('Plate.addPluginsAsync() should add plugins without error', (done) => {
    Plate.addPluginsAsync(plugins, (error, results) => {
      expect(error).to.be.null
      expect(results).to.have.lengthOf(2)
      done()
    })
  })
})

describe('Get a list of plugins', () => {
  it('Plate.getPluginsAsync() should return all plugins', (done) => {
    Plate.getPluginsAsync((error, plugins) => {
      expect(error).to.be.null
      expect(plugins).to.be.lengthOf(4)
      expect(plugins).to.contain('test1')
      expect(plugins).to.contain('test2')
      expect(plugins).to.contain('test3')
      expect(plugins).to.contain('test4')
      done()
    })
  })
})

describe('Running plugins', () => {
  var Eat
  it('Plate.resolveHookAsync() should create a new Eat class without error', (done) => {
    Plate.resolveHookAsync('hook', (error, eater) => {
      expect(error).to.be.null
      expect(eater).to.have.property('hook')
        .with.property('plugins')
        .with.a.lengthOf(4)
      Eat = eater
      done()
    })
  })

  for (var i=0; i<4; i++) {
    it('Eat.nextAsync() should iterate over the plugins', (done) => {
      Eat.nextAsync((error, plugin) => {
        expect(error).to.be.null
        expect(plugin.handler()).to.be.oneOf(['test1', 'test2', 'test3', 'test4'])
        done()
      })
    })
  }
})

describe('Verifying dependencies', () => {
  it('Plate.verifyDependenciesAsync() should return an empty list', (done) => {
    Plate.verifyDependenciesAsync((error, data) => {
      expect(error).to.be.null
      expect(data).to.be.empty
      done()
    })
  })

  it('Plate.verifyDependenciesAsync() should return a list with missing plugins', (done) => {
    Plate.addPlugin({
      name: 'test5',
      'hook': function() { return true },
      dependencies: ['non-existant-plugin']
    })
    Plate.verifyDependenciesAsync((error, data) => {
      expect(error).to.be.null
      expect(data).to.contain('non-existant-plugin')
      done()
    })
  })
})

describe('Error checking for incorrect values', () => {
  describe('Plate.addPluginAsync() should throw where plugin is not an object', () => {
    it('~function should throw TypeError', (done) => {
      Plate.addPluginAsync(function() {}, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~string should throw TypeError', (done) => {
      Plate.addPluginAsync('string', (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~number should throw TypeError', (done) => {
      Plate.addPluginAsync(123, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.addPluginAsync(true, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        Plate.addPluginAsync(false, (error) => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
      })
    })

    it('~null should throw TypeError', (done) => {
      Plate.addPluginAsync(null, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~array should throw TypeError', (done) => {
      Plate.addPluginAsync(['wrong'], (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.addPluginAsync(undefined, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~object should not throw TypeError', (done) => {
      Plate.addPluginAsync({ name: 'test' }, (error) => {
        expect(error).to.be.null
        done()
      })
    })
  })

  describe('Plate.addPluginsAsync() should throw where plugins is not an array', () => {
    it('~string should throw TypeError', (done) => {
      Plate.addPluginsAsync('wrong', (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~number should throw TypeError', (done) => {
      Plate.addPluginsAsync(123, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~object should throw TypeError', (done) => {
      Plate.addPluginsAsync({}, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.addPluginsAsync(false, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        Plate.addPluginsAsync(true, (error) => {
          expect(error).to.be.an.instanceof(TypeError)
          done()
        })
      })
    })

    it('~function should throw TypeError', (done) => {
      Plate.addPluginsAsync(function() {}, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~null should throw TypeError', (done) => {
      Plate.addPluginsAsync(null, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.addPluginsAsync(undefined, (error) => {
        expect(error).to.be.an.instanceof(TypeError)
        done()
      })
    })

    it('~array should not throw TypeError', (done) => {
      Plate.addPluginsAsync([{ name: 'test' }], (error) => {
        expect(error).to.be.null
        done()
      })
    })
  })

  describe('Plate.resolveHookAsync() should throw where hook is not a string', () => {
    it('~null should throw TypeError', (done) => {
      Plate.resolveHookAsync(null, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })

    it('~undefined should throw TypeError', (done) => {
      Plate.resolveHookAsync(undefined, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })

    it('~boolean should throw TypeError', (done) => {
      Plate.resolveHookAsync(true, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        Plate.resolveHookAsync(false, (error) => {
          expect(error).to.be.an.instanceOf(TypeError)
          done()
        })
      })
    })

    it('~object should throw TypeError', (done) => {
      Plate.resolveHookAsync({}, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })

    it('~array should throw TypeError', (done) => {
      Plate.resolveHookAsync(['wrong'], (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })

    it('~number should throw TypeError', (done) => {
      Plate.resolveHookAsync(123, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })

    it('~function should throw TypeError', (done) => {
      Plate.resolveHookAsync(function() {}, (error) => {
        expect(error).to.be.an.instanceOf(TypeError)
        done()
      })
    })
  })
})
