# 1.0.0

- Initial program

# 1.1.0

- Added asynchronous support, callback and promises
- Completed testing for asynchronous support
- Fixings.addPlugin() now returns true if successful or false (plugin already exists)
- Fixings.addPlugins() now returns a list of objects, with the name of the plugin, and whether or not it was added
- Added projects url to package.json

# 1.1.1

- Converted setTimeout's to setImmediate's 
- Checks to see if hook exits before returning an Eater. Should one not exist, returns null.