/**
 * Fixings Master file.
 *
 * Contains Eat and Fixings classes. Create a new Fixings class
 * and add support for plugins for your coding project.
 *
 * @version 1.0.0
 * @author Drew Sommer
 * @license MIT
 */

module.exports = require('./src/fixings')
