var expect = require('chai').expect

describe('Synchronous testing', () => {
  require('./tests/index.sync.spec')
})

describe('Asynchronous testing', () => {
  require('./tests/index.async.spec')
})
