/**
 * @property {PluginHook[]} plugins - A list of plugins for the hook
 * @property {Number[]} order - The index numbers the for plugin run order
 */
var Hook = {} // eslint-disable-line no-unused-vars
/**
 * @property {Array=} $before - List of plugins to run after this plugin
 * @property {Array=} $after - List of plugins to run before this plugin
 * @property {Function} $handler - Plugins hook function
 */
var PluginHook = {} // eslint-disable-line no-unused-vars
/**
 * @property {String} $name - plugin name
 * @property {(PluginHook|Function)=} 'hook' - A hook the plugin registers
 *   mutliple hooks as separate 'hook-names'
 * @property {Array=} $dependencies - A list of plugins that this plugin requires
 * @todo make it so $dependencies can be marked optional
 */
var Plugin = {} // eslint-disable-line no-unused-vars